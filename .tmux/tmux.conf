# ==========================
# ===  General Settings  ===
# ==========================

#set-option -sa terminal-overrides ",$TERM:RGB"
#set-option -ga terminal-overrides ",$TERM:Tc"
#set -g default-terminal "screen-256color"
#set -g default-terminal "tmux-256color"
set -g history-limit 5000
set -g buffer-limit 20
set -sg escape-time 10
set -g display-time 1000
setw -g remain-on-exit off
set -g repeat-time 600
setw -g allow-rename on
setw -g automatic-rename on
setw -g aggressive-resize off

set -as terminal-overrides ',*:Smulx=\E[4::%p1%dm'  # undercurl support
set -as terminal-overrides ',*:Setulc=\E[58::2::%p1%{65536}%/%d::%p1%{256}%/%{255}%&%d::%p1%{255}%&%d%;m'  # underscore colors - needs tmux-3.0

# Change prefix key to C-a, easier to type, same as "screen"
unbind C-b
set -g prefix C-a

# Start index of window/pane with 1, because we're humans, not computers
set -g base-index 1
setw -g pane-base-index 1

# Enable mouse support
set -g mouse on


# Bindings
set -g prefix C-a
bind r source-file ~/.tmux.conf \; display '~/.tmux.conf sourced'

#bind-key -n Home send Escape "OH"
#bind-key -n End send Escape "OF"

# ==========================
# ===    Key Bindings    ===
# ==========================

# Unbind default key bindings, we're going to override
unbind %    # split-window -h
unbind '"'  # split-window
unbind l    # last-window
unbind &    # kill-window
unbind =    # choose-buffer
unbind ]    # paste-buffer

# Navigation
bind -n M-Right next-window
bind -n M-Left previous-window

# Synchronization
#bind-key -n F2 set-window-option synchronize-panes

# Window/Session
bind-key n command-prompt -p "rename-window:" -I "#{window_name}" "rename-window '%%'"
bind-key N command-prompt -p "rename-session:" -I "#{session_name}" "rename-session '%%'"
bind - split-window -v
bind _ split-window -h
bind > swap-pane -D       # swap current pane with the next one
bind < swap-pane -U       # swap current pane with the previous one
bind C-c new-session
bind Tab last-window
bind C-a last-window
bind b send-prefix
bind X confirm-before -p "kill-window #W? (y/n)" kill-window

# ================================================
# ===     Copy mode, scroll and clipboard      ===
# ================================================
#set -g @copy_use_osc52_fallback off
set -s set-clipboard on
#set -as terminal-features ',screen-256color:clipboard'
set -sa terminal-features ",alacritty*:256:RGB:bpaste:clipboard:mouse:strikethrough:title:ccolour:cstyle:focus:overline:usstyle:hyperlinks:sync"

# Prefer vi style key table
setw -g mode-keys vi

bind p paste-buffer
bind C-p choose-buffer

# trigger copy mode by (not working for me)
#bind -n M-Up copy-mode

yank="~/.tmux/yank.sh"

# Copy selected text (removed for testing)
#bind -T copy-mode-vi Enter send-keys -X copy-pipe-and-cancel "$yank"
#bind -T copy-mode-vi y send-keys -X copy-pipe-and-cancel "$yank"
##bind -T copy-mode-vi Y send-keys -X copy-line \;\
##  run "tmux save-buffer - | $yank"
#bind -T copy-mode-vi Y send-keys -X copy-pipe-and-cancel "$yank; tmux paste-buffer"
#bind -T copy-mode-vi C-j send-keys -X copy-pipe-and-cancel "$yank"
#bind-key -T copy-mode-vi D send-keys -X copy-end-of-line \;\
#  run "tmux save-buffer - | $yank"
#bind-key -T copy-mode-vi A send-keys -X append-selection-and-cancel \;\
#  run "tmux save-buffer - | $yank"

## Copy selection on drag end event, but do not cancel copy mode and do not clear selection
## clear select on subsequence mouse click
##bind -T copy-mode-vi MouseDragEnd1Pane \
##  send-keys -X copy-pipe "$yank"
#unbind -T copy-mode-vi MouseDragEnd1Pane
#bind -T copy-mode-vi MouseDown1Pane select-pane \;\
#  send-keys -X copy-pipe "$yank" \;\
#  send-keys -X clear-selection
# End (removed for testing) Section


set -g bell-action any
set -g default-command ""
set -g default-shell "/bin/zsh"
set -g destroy-unattached off
set -g detach-on-destroy on
set -g display-panes-active-colour "#00afff"
set -g display-panes-colour "#00afff"
set -g display-panes-time 800
set -g renumber-windows on
set -g status-keys vi
set -g status-interval 4
set -g visual-activity off
set -g visual-bell off
set -g visual-silence off

setw -g alternate-screen on
setw -g clock-mode-style 24
# Removed in 2.9
#setw -g force-height 0
#setw -g force-width 0
setw -g main-pane-height 24
setw -g main-pane-width 80
setw -g monitor-activity on
setw -g monitor-bell on
setw -g monitor-silence 0
setw -g other-pane-height 0
setw -g other-pane-width 0

setw -g wrap-search on
setw -g xterm-keys on


set -g status-position top
set -g set-titles on
set -g set-titles-string "#h ❐ #S ● #I #W"


set -g status-left-length 1000
set -g status-right-length 1000

set -g status-left "#[fg=#000000,bg=#ffff00,bold] ❐ #S #[fg=#ffff00,bg=default,none] "
set -g status-right '#[fg=#000000]#{prefix}#{synchronized}#{sharedsession}#[fg=#8a8a8a,bg=#000000]#[fg=#e4e4e4] #{cpu_icon} #[fg=#8a8a8a]#[fg=#e4e4e4] %R #[fg=#8a8a8a]#[fg=#e4e4e4] %d %b #[fg=#d70000]#[fg=#ffffff,bg=#d70000] #{username} #[fg=#e4e4e4,bg=#d70000,bold]#[fg=#000000,bg=#e4e4e4] #{hostname} #[bg=default]'

set -g status-style "fg=#8a8a8a,bg=#080808"
set -g status-left-style "fg=#8a8a8a,bg=#080808"
set -g status-right-style "fg=#8a8a8a,bg=#080808"

set -g message-style "fg=#000000,bg=#ffff00,bright"
set -g message-command-style "fg=#ffff00,bg=#000000,bright"

setw -g automatic-rename-format "#{?pane_in_mode,[tmux],#{pane_current_command}}#{?pane_dead,[dead],}"
setw -g clock-mode-colour "#00afff"
setw -g mode-style "fg=#000000,bg=#ffff00,bright"
setw -g pane-active-border-style "fg=#00afff"
setw -g pane-border-format "#{?pane_active,#[reverse],}#{pane_index}#[default] \"#{pane_title}\""
setw -g pane-border-status off
setw -g pane-border-style "fg=#444444"

setw -g window-style default
setw -g window-active-style "bg=default"
#setw -g window-active-style "bg=#000000"
setw -g window-status-format "#I #W"
setw -g window-status-style "fg=#8a8a8a,bg=#080808"
setw -g window-status-last-style "fg=#00afff,bg=#080808,bright"
setw -g window-status-activity-style underscore
setw -g window-status-bell-style "fg=#ffff00,bright,blink"
#setw -g window-status-current-format "#[fg=#080808,bg=#00afff]#[fg=default,bg=default,default] #I #W #[fg=#00afff,bg=#080808,none]"
setw -g window-status-current-format "#[fg=#080808,bg=#00afff]#[fg=#080808,bg=#00afff,bright] #I #W #[fg=#00afff,bg=#080808,none]"
#setw -g window-status-current-style "fg=#000000,bg=#00afff,bright"
setw -g window-status-separator " "


# ============================
# ===       Plugins        ===
# ============================
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-cpu'
set -g @plugin 'tmux-plugins/tmux-yank'
set -g @plugin 'https://git.linux-help.org/psi-jack/tmux-host.git'
set -g @plugin 'https://git.linux-help.org/psi-jack/tmux-indicators.git'

set -g @yank_action 'copy-pipe'

# =============================================
# ===   Nesting local and remote sessions   ===
# =============================================

# Load modular configuration
if-shell 'test -n "$SSH_CLIENT"' \
    'source-file ~/.tmux/tmux.remote.conf' \
    'source-file ~/.tmux/tmux.local.conf'

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run -b '~/.tmux/plugins/tpm/tpm'

