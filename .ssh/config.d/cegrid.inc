Host ceweb
    Hostname jand.dyndns.biz
    Port 22
    user mykael
Host cewebproxy
    Hostname jand.dyndns.biz
    Port 22
    user mykael
    DynamicForward 5534
Host cebackdoor
    Hostname jand.dyndns.biz
    Port 2022
    user mykael
Host cerobust1
    Hostname robust1
    user mykael
    ProxyJump ceweb
Host cerobust2
    Hostname 192.168.1.52
    user mykael
    ProxyJump ceweb
Host ceregions1
    Hostname regions1
    user mykael
    ProxyJump ceweb
Host ceregions2
    Hostname regions2
    user mykael
    ProxyJump ceweb
Host ceregions3
    Hostname regions3
    user mykael
    ProxyJump ceweb
Host ceregions4
    Hostname regions4
    user mykael
    ProxyJump ceweb
#Host cegrid
#    Hostname jand.dyndns.biz
#    Port 23
#    User mykael
Host *.cegrid.ld
    User mykael

