if (( ${+commands[kubectl]} )); then
  alias k=kubectl
  source <(kubectl completion zsh)
fi
