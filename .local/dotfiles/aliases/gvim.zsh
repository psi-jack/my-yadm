case `uname` in
  Linux)
    if whence -p nvim-gtk > /dev/null; then
      alias vim=nvim-gtk
    elif whence -p nvim > /dev/null; then
      alias vim=nvim
    elif whence -p gvim > /dev/null; then
      alias vim=gvim
    fi
    ;;
  Darwin)
    if whence -p nvim > /dev/null; then
      alias vim=nvim
    fi
esac

