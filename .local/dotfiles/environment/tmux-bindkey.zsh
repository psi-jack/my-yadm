#bindkey -s "\e[24;3~" ""
#bindkey -s "\e[1;2A" ""
#bindkey -s "\e[1;2B" ""

# for non RH/Debian xterm, can't hurt for RH/DEbian xterm
bindkey "eOH" beginning-of-line
bindkey "eOF" end-of-line
# for freebsd console
bindkey "e[H" beginning-of-line
bindkey "e[F" end-of-line
