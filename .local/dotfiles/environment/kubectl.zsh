if [[ -d "$HOME/.krew/bin" || -n "$KREW_ROOT" ]]; then
  export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
fi

