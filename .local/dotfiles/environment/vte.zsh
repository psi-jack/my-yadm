if [[ -f /etc/solus-release ]]; then                                                                                                                                                           
    export SSH_ASKPASS=/usr/lib64/seahorse/seahorse/ssh-askpass                                                                                                                                
                                                                                                                                                                                               
    if [[ $TILIX_ID || $VTE_VERSION ]]; then                                                                                                                                                   
        source /usr/share/defaults/etc/profile.d/vte.sh                                                                                                                                        
        precmd_functions+=(__vte_osc7)                                                                                                                                                         
    fi                                                                                                                                                                                         
elif [[ -f /etc/debian_version ]]; then                                                                                                                                                        
  if [[ $TILIX_ID || $VTE_VERSION ]]; then                                                                                                                                                     
    if [[ -r "/etc/profile.d/vte-2.91.sh" ]]; then                                                                                                                                             
      source /etc/profile.d/vte-2.91.sh                                                                                                                                                        
      precmd_functions+=(__vte_osc7)                                                                                                                                                           
    fi                                                                                                                                                                                         
  fi                                                                                                                                                                                           
elif [[ -f /etc/fedora-release ]]; then                                                                                                                                                        
  if [[ $TILIX_ID || $VTE_VERSION ]]; then
    if [[ -r "/etc/profile.d/vte.sh" ]]; then
      source /etc/profile.d/vte.sh
      precmd_functions+=(__vte_osc7)
    fi
  fi
fi

