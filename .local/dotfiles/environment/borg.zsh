case "$(hostname -s)" in
  midgaard)       BORG_REPO="ssh://psi-jack@mega.home.ld/volume1/homes/psi-jack/borg-midgaard";;
  loki)           BORG_REPO="ssh://psi-jack@mega.home.ld/volume1/homes/psi-jack/borg-loki";;
  ragnarok)       BORG_REPO="ssh://psi-jack@mega.home.ld/volume1/homes/psi-jack/borg-ragnarok";;
  Intelity-02110) BORG_REPO="ssh://psi-jack@mega.home.ld/volume1/homes/psi-jack/borg-worktop";;
  *)              BORG_REPO="ssh://psi-jack@mega.home.ld/volume1/homes/psi-jack/borg";;
esac
export BORG_REMOTE_PATH="/usr/local/bin/borg"

#export BORG_REPO=mega:/volume1/homes/psi-jack/borg
#export BORG_REPO=psi-jack@mega-backups:/volume1/homes/psi-jack/borg
#export BORG_PASSCOMMAND="pass print borg --please_show_me_the_passwords"
