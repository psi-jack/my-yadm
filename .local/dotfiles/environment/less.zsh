export LESS_TERMCAP_mb=$'\e[1;31m'        # enter blinking mode
#export LESS_TERMCAP_md=$'\e[1;34;74m'     # double-bright mode
export LESS_TERMCAP_md=$'\e[38;5;106m'     # double-bright mode
export LESS_TERMCAP_me=$'\e[0m'           # turn off all appearance mode

#export LESS_TERMCAP_so=$'\e[30;42m'       # enter standout mode
export LESS_TERMCAP_so=$'\e[38;5;23;48;5;106m'      # begin standout-mode - info box
export LESS_TERMCAP_se=$'\e[0m'           # end standout mode

export LESS_TERMCAP_us=$'\e[4;32;146m'    # begin underline
export LESS_TERMCAP_ue=$'\e[0m'           # end underline

#export LESS_TERMCAP_so=$(tput bold; tput setaf 3; tput setab 4) # yellow on blue
#export LESS_TERMCAP_se=$(tput rmso; tput sgr0)

#export LESS_TERMCAP_mr=$(tput rev)
#export LESS_TERMCAP_mh=$(tput dim)

export LESS="-R -x2 -I"
export LESSOPEN="|~/bin/lessfilter %s"
export PYGMENTIZE_STYLE='lovelace'

#if [[ -f "/usr/share/nvim/runtime/macros/less.sh" ]]; then
#  alias less="/usr/share/nvim/runtime/macros/less.sh"
#elif [[ -f "/usr/share/vim/vim80/macros/less.sh" ]]; then
#  alias less="/usr/share/vim/vim80/macros/less.sh"
#elif [[ -f "/usr/share/vim/vim74/macros/less.sh" ]]; then
#  alias less="/usr/share/vim/vim74/macros/less.sh"
#fi

function less() {
  if [ -t 1 ]; then
    if test $# = 0; then
      if test -t 0; then
        echo "Missing filename" 1>&2
        exit
      fi
      /usr/bin/less -
    else
      if [[ -f "/usr/share/nvim/runtime/macros/less.sh" ]]; then
        /usr/share/nvim/runtime/macros/less.sh "$@"
      elif [[ -f "/usr/share/vim/vim80/macros/less.sh" ]]; then
        /usr/share/vim/vim80/macros/less.sh "$@"
      elif [[ -f "/usr/share/vim/vim74/macros/less.sh" ]]; then
        /usr/share/vim/vim74/macros/less.sh "$@"
      else
        /usr/bin/less "$@"
      fi
    fi
  else
    if test $# = 0; then
      if test -t 0; then
        echo "Missing filename" 1>&2
        exit
      fi
      cat
    else
      cat "$@"
    fi
  fi
}
