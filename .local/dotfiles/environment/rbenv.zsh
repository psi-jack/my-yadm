which rbenv &>/dev/null
if [[ $? -eq 0 ]]; then
  export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$HOME/.rbenv/bin:$PATH"
  eval "$(rbenv init -)"
fi

