function lessfilter ()
{
  case "$1" in
    *.awk|*.groff|*.java|*.js|*.m4|*.php|*.pl|*.pm|*.pod|*.sh|\
    *.ad[asb]|*.asm|*.inc|*.[ch]|*.[ch]pp|*.[ch]xx|*.cc|*.hh|\
    *.lsp|*.l|*.pas|*.p|*.xml|*.xps|*.xsl|*.axp|*.ppd|*.pov|\
    *.diff|*.patch|*.py|*.rb|*.sql|*.ebuild|*.eclass)
      if whence -p pygmentize > /dev/null; then
        pygmentize -f 256 "$1"
      else
        cat "$1"
      fi
      ;;
    .bashrc|.bash_aliases|.bash_environment)
      if whence -p pygmentize > /dev/null; then
        pygmentize -f 256 -l sh "$1"
      else
        cat "$1"
      fi
      ;;
    *)
      if grep -q "#\!/bin/bash" "$1" 2>/dev/null; then
        pygmentize -f 256 -l sh "$1"
      else
        exit 1
      fi
      ;;
  esac
}

