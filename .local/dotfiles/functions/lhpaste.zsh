function lhpaste() {
  local server_url="https://paste.linux-help.org/"
  local expire="1day"

  #zparseopts -D -E -F - 

  pbcli --host="$server_url" --expire="$expire"
}
