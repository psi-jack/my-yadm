function reload_dotfiles ()
{
	while read f
	do
		source "$f"
  done < <(find "${HOME}/.local/dotfiles" \( -type l -o -type f \) -name '*.zsh')
}

